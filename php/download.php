<?php


$upload_id = $Frisky_action->get_var('upload_id');

$data = $Frisky_action->sql->query("SELECT passive_name FROM uploads WHERE upload_id = '$upload_id'");
$row = $data->fetch_array();
$filename = 'magic/inc/uploads/'.$row['passive_name'];

$mimetype='application/octet-stream';
// Отправляем требуемые заголовки
header ($_SERVER["SERVER_PROTOCOL"] . ' 200 OK');
// Тип содержимого. Может быть взят из заголовков полученных от клиента
// при закачке файла на сервер. Может быть получен при помощи расширения PHP Fileinfo.
header ('Content-Type: ' . $mimetype);
// Дата последней модификации файла       
header ('Last-Modified: ' . gmdate ('r', filemtime ($filename)));
// Отправляем уникальный идентификатор документа,
// значение которого меняется при его изменении.
// В нижеприведенном коде вычисление этого заголовка производится так же,
// как и в программном обеспечении сервера Apache
header ('ETag: ' . sprintf ('%x-%x-%x', fileinode ($filename), filesize ($filename), filemtime ($filename)));
// Размер файла
header ('Content-Length: ' . (filesize ($filename)));
header ('Connection: close');
// Имя файла, как он будет сохранен в браузере или в программе закачки.
// Без этого заголовка будет использоваться базовое имя скрипта PHP.
// Но этот заголовок не нужен, если вы используете mod_rewrite для
// перенаправления запросов к серверу на PHP-скрипт
header ('Content-Disposition: attachment; filename="' . basename ($filename) . '";');
$f = fopen ($filename, 'r');
while (!feof($f)) {
// Читаем килобайтный блок, отдаем его в вывод и сбрасываем в буфер
echo fread($f, 1024);
flush();
}
// Закрываем файл
fclose ($f);
?>