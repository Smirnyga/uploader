<?php

$output_dir = "../temp/";
$itera = $_POST['itera'];
$flag_chunk = $_POST['end'];
$file_now_name = $_POST['file_name'];
$file_set_dir = $_POST['file_dir'];

$file_get_name = sha1($file_now_name);
if ($file_set_dir == '') {
    $index_dir = 1;
    $skip = array('.', '..');
    $files_get = scandir($output_dir);
    foreach ($files_get as $dir) {
        if (!in_array($dir, $skip)) {
            $pos_end = strpos($dir, '_');
            $new_str_file = mb_substr($dir, 0, $pos_end + 1);
            if ($new_str_file == $file_get_name . '_') {
                $index_dir++;
            }
        }
    }
    $file_set_dir = $file_get_name . '_' . $index_dir;
	
	
    mkdir($output_dir . $file_set_dir);


}

$json_answer['file_dir'] = $file_set_dir;

$output_dir = $output_dir . $file_set_dir . "/";

$new_name = explode('.', $_FILES["upload"]["name"]);


$file_type = $_FILES["upload"]["type"];
$fileName = $itera . '.' . end($new_name);
move_uploaded_file($_FILES["upload"]["tmp_name"], $output_dir . $fileName);

if ($flag_chunk == $itera) {

    $buffer = "";
  	
	 if($handle = opendir($output_dir)) {
  $files = array();
    while (false !== ($file = readdir($handle))) {
	if($file != "." && $file != "..") {
	$files[] = $file;
	}
    }
    closedir($handle); 
}
sort($files, SORT_NUMERIC);
	
	
	
	
	for ($k = 0; $k < count($files); $k++) {
                $buffer .= file_get_contents($output_dir.$files[$k]);
				unlink($output_dir.$files[$k]);
        }

    $new_name_save = explode('.', $file_now_name);
    $name_save = md5(rand(1, 1000) . time()) . '.' . end($new_name_save);

    $fp = fopen("../uploads/" . $name_save, "w");
    fwrite($fp, $buffer);
    fclose($fp);
   rmdir($output_dir);

    $is_img = 0;
    if (getimagesize("../uploads/" . $name_save)) {
        $is_img = 1;
    }
	$json_answer['file_loaded'] = true;
    $json_answer['file_name'] = $name_save;
    $json_answer['is_img'] = $is_img;
    $json_answer['true_file_name'] = $file_now_name;
	$size = filesize("../uploads/" . $name_save);
	$ext = end($new_name_save);
	$json_answer['file_size'] = $size;
	$json_answer['file_type'] = $ext;
	$json_answer['file_date'] = date("Y-m-d");

}


echo json_encode($json_answer);
?>