<?php
include("magic/inc/check_admin.php"); 
    $output_dir = "magic/inc/uploads/";
    $clear_del = $Frisky_action->clear_data($_POST['action'], $_POST['file_dir'],
        $_POST['file_id']);

    function deleteDirectory($dir)
    {
        if (!file_exists($dir))
            return true;
        if (!is_dir($dir) || is_link($dir))
            return unlink($dir);
        foreach (scandir($dir) as $item) {
            if ($item == '.' || $item == '..')
                continue;
            if (!deleteDirectory($dir . "/" . $item)) {
                chmod($dir . "/" . $item, 0777);
                if (!deleteDirectory($dir . "/" . $item))
                    return false;
            }
            ;
        }
        return rmdir($dir);
    }


    if ($clear_del[0] == 1) {
        if (!empty($clear_del[2])) {
            $file_id = (int)$clear_del[2];
            $data = $Frisky_action->sql->query("SELECT passive_name FROM uploads WHERE upload_id = $file_id");
            if ($data->num_rows) {
                $row = $data->fetch_array();
                $filePath = $output_dir .  $row['passive_name'];
                if (file_exists($filePath)) {
                    unlink($filePath);
                }
                $Frisky_action->sql->query("DELETE FROM uploads WHERE upload_id = $file_id");
            }
        }
    } else {
        if (!empty($clear_del[1])) {
            $dir_del = $clear_del[1];
            deleteDirectory("temp/" . $dir_del);
        }
    }

?>