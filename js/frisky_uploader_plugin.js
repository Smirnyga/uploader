/*
если же вы планируете загружать файлы большого размера, вам необходимо в файле php.ini повысить значения таких параметров, как:
post_max_size- максимально возможный размер файла при post запросе.( изменять нужно только цифру, букву M не трогать)
upload_max_filesize- максимальный размер файла при запросе любого типа( должен быть всегда меньше чем post_max_size)
Удачи!*/

Array.prototype.removeByValue = function(val) {
    for(var i=0; i<this.length; i++) {
        if(this[i] == val) {
            this.splice(i, 1);
            break;
        }
    }
}	

function Frisky_uploader(param) {

this.settings = { 
maxSize : 1024*1024*1024*1024,
maxFiles : '*',
extList : 'jpg,rar,png,jpeg,zip,avi,ppt,pptx,docx,doc,mp3,7z,txt,flv,mp4',
root_dir : '',
upload_php : ''
};

this.is_upload = true;
this.storage = new Array();


for(property in param) {
this.settings[property] = param[property];	
}	

this.settings.extList = String(this.settings.extList).split(',');

}
	
Frisky_uploader.prototype.error = function(anon){
this.error_function = anon; 
};

Frisky_uploader.prototype.load_complete = function(anon){
this.load_complete_function = anon; 
};

Frisky_uploader.prototype.upload_abort = function() { 
this.self_ajax.abort();
$('.uploaded').removeClass("uploaded").addClass("loaded");
this.is_upload = true;
if(this.storage.length > 0) { 
this.upload_ajax();
} 

};


Frisky_uploader.prototype.is_error = function(anon) { 
this.is_error_function = anon;
}

Frisky_uploader.prototype.upload_ajax_chunk = function(chunk, file, c, count, start, end, bytes_chunk) { 

percent_blob = 0;
var div_wd = 100/count;
var form = new FormData(); 
form.append('upload', chunk);
form.append('itera', c);
form.append('end', count);
form.append('file_name', file.name);
form.append('file_dir', this.file_dir);

self = this;
this.self_ajax = $.ajax({
url: self.settings.upload_php,
type: 'post',
data: form,
dataType: 'json',
cache: false,
contentType: false,
processData: false,
forceSync: false,
xhr: function(){
var req = $.ajaxSettings.xhr();
if(req.upload){
req.upload.addEventListener('progress', function(event) {
var percent = 0;
var position = event.loaded || event.position;
var total = event.total || event.totalSize;
if(event.lengthComputable){
percent = position / total * 100;
percent_blob = div_wd * percent / 100;
percent_blob = self.percent_all + percent_blob;
$('.uploaded').width(percent_blob+'%');
$('.uploaded').text((percent_blob - (percent_blob%1))+'%');	
}		

}, false);	

}
return req;

},
success: function (data, message, xhr){

self.percent_all = percent_blob;
if(self.file_dir == '') { 
self.file_dir = data.file_dir;
}

if('file_loaded' in data) {
$('#abort_upload').remove();
self.load_complete_function(data, file);
$('.uploaded').removeClass("uploaded").addClass("loaded");
//alert('Отладочное сообщение об успешной загрузке');
self.is_upload = true;
if(self.storage.length > 0) { 
self.upload_ajax();
} 
}
else
{
self.set_chunk(start, end, bytes_chunk, file, count);
}

},
error: function (jqXHR, textStatus, errorThrown){ 
self.is_error_function(file);
}


});


};


Frisky_uploader.prototype.upload_ajax = function() {
if(this.is_upload == true) { 
$('.prepare_upload').filter(':first').remove();
var file = this.storage.shift();
this.status_upload_function(file);
this.is_upload = false;
this.percent_all = 0;
this.file_dir = '';
var start = 0;
var bytes_chunk = 1024*1024*1;
var end = bytes_chunk;
var count = Math.ceil(file.size / bytes_chunk);
this.c = 1;
this.set_chunk(start, end, bytes_chunk, file, count);

}
};


Frisky_uploader.prototype.set_chunk = function(start, end, bytes_chunk, file, count) {

var chunk = this.check_slice(start, end, file);
start = end;
end = start + bytes_chunk; 

this.upload_ajax_chunk(chunk, file, this.c++, count, start, end, bytes_chunk);


}


Frisky_uploader.prototype.check_slice = function(start_slice, end_slice, file) {
if(file.slice){ 
var chunk = file.slice(start_slice, end_slice);
} else {
if (file.webkitSlice) {
var chunk = file.webkitSlice(start_slice, end_slice);
} else {
if (file.mozSlice) {
var chunk = file.mozSlice(start_slice, end_slice);
}
}
}
return chunk;

};

Frisky_uploader.prototype.status_prepare = function(anon){
this.status_prepare_function = anon; 
};

Frisky_uploader.prototype.status_upload = function(anon){
this.status_upload_function = anon; 
};
 

Frisky_uploader.prototype.uploadFile = function(files) {
 
for(i = 0, l = files.length; i < l; i++) { 

var ext = files[i].name.toLowerCase().split('.').pop();

if (files[i].size > this.settings.maxSize) {
this.error_function(1, files[i]);
continue;
}

if ($.inArray(ext, this.settings.extList) < 0) {
this.error_function(2, files[i]);
continue;
}


this.storage.push(files[i]);
this.status_prepare_function(files[i]);


}


if(this.storage.length > 0) {
this.upload_ajax();
}


};